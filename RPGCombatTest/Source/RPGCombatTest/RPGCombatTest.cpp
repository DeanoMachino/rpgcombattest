// Copyright Epic Games, Inc. All Rights Reserved.

#include "RPGCombatTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, RPGCombatTest, "RPGCombatTest" );

DEFINE_LOG_CATEGORY(LogRPGCombatTest)
 