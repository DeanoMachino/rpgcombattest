// Copyright Epic Games, Inc. All Rights Reserved.

#include "RPGCombatTestGameMode.h"
#include "RPGCombatTestPlayerController.h"
#include "RPGCombatTestCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARPGCombatTestGameMode::ARPGCombatTestGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ARPGCombatTestPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}