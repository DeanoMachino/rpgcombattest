// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RPGCombatTestGameMode.generated.h"

UCLASS(minimalapi)
class ARPGCombatTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARPGCombatTestGameMode();
};



